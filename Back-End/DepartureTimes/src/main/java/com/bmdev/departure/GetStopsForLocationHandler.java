package com.bmdev.departure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Queue;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;

/**
 * This lambda function retrieves a list of COUNT bus stations (stops) near a
 * given location in a radius of RADIUS. The Bus stations are sorted in
 * increasing order of the distance between the location and the station. The
 * information on the location of the stops are retrieved from a database
 * pre-populated with static bus schedule information from the Next-Bus API.
 * <br>
 * <br>
 * The function expects an input stream containing a GET request in the form of
 * a {@link JSONObject}. The location is specified in the queryStringParameters
 * of the request as "lat" and "lon". <br>
 * <br>
 * The database it connects to is {@link DynamoDB} no-sql database containing
 * tables Agency and Stops. The database must be hosted in the same region as
 * the lambda function on AWS.<br>
 * <br>
 * To obtain the list of stops we scan through the db for values in a square
 * around the location and then filter them in a circle with radius RADIUS
 * afterwards when there are significantly less to process.<br>
 * <br>
 * We return the list of stops in the body of a GET response sent from the
 * function to the client via the {@link OutputStream} passed to it. If an error
 * occurred the function returns an error response with the message in the
 * "exception" entry of the response.<br>
 * <br>
 * 
 * @author Heila Botha
 *
 */
public class GetStopsForLocationHandler implements RequestStreamHandler {
	private static final Region REGION = Region.getRegion(Regions.US_EAST_1);

	/** Builder for Amazon DynamoDB clients */
	private static final AmazonDynamoDBClientBuilder DB_CLIENT_BUILDER = AmazonDynamoDBClientBuilder.standard()
			.withRegion(REGION.getName());

	/** Radius of the earth */
	private static final double R = 6371;

	private static LambdaLogger logger;

	private double radius = 1;
	private int numStops = 7;

	@Override
	public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
		logger = context.getLogger();
		logger.log("Running Lambda function GetStopsForLocationHandler");

		double lat = -1;
		double lon = -1;

		JSONParser parser = new JSONParser();
		JSONObject response = getJSONResponse();

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			JSONObject event = (JSONObject) parser.parse(reader);
			if (event.get("queryStringParameters") != null) {
				JSONObject qps = (JSONObject) event.get("queryStringParameters");
				if (qps.get("lat") != null) {
					lat = Double.parseDouble((String) qps.get("lat"));
				}
				if (qps.get("lon") != null) {
					lon = Double.parseDouble((String) qps.get("lon"));
				}
				if (qps.get("radius") != null) {
					radius = Double.parseDouble((String) qps.get("radius"));
				}
				if (qps.get("count") != null) {
					numStops = Integer.parseInt((String) qps.get("count"));
				}
			}
			if (lat == -1 || lon == -1) {
				setErrorResponse(response, "No latitude and longitude specified");
				sendResponse(response, output);
				return;
			}

			logger.log("Parsed lat: " + lat + " lon: " + lon + " radius:" + radius + " numStops:" + numStops);

			DynamoDB db = connectToDB();
			Queue<Stop> stops = getStops(db, lon, lat);

			addStopsToResponse(response, stops);

		} catch (ParseException pex) {
			setErrorResponse(response, pex);
		}

		sendResponse(response, output);
	}

	@SuppressWarnings("unchecked")
	private void setErrorResponse(JSONObject response, String string) {
		response.put("statusCode", "400");
		response.put("exception", string);
	}

	@SuppressWarnings("unchecked")
	private void setErrorResponse(JSONObject response, ParseException pex) {
		response.put("statusCode", "400");
		response.put("exception", pex);
	}

	@SuppressWarnings({ "unchecked" })
	private void addStopsToResponse(JSONObject response, Queue<Stop> stops) {
		JSONArray stopsArray = new JSONArray();
		int count = numStops;
		for (Stop s : stops) {
			if (count-- > 0) {
				stopsArray.add(s.toJson());
				logger.log("Adding stop " + s);
			} else {
				break;
			}
		}

		JSONObject responseBody = new JSONObject();
		responseBody.put("stops", stopsArray);
		response.put("body", responseBody.toString());
	}

	@SuppressWarnings("unchecked")
	private JSONObject getJSONResponse() {
		JSONObject responseJson = new JSONObject();
		responseJson.put("statusCode", "200");

		JSONObject headerJson = new JSONObject();
		headerJson.put("Access-Control-Allow-Origin", "*");
		headerJson.put("Content-Type", "application/json");

		responseJson.put("headers", headerJson);

		return responseJson;
	}

	private void sendResponse(JSONObject response, OutputStream output) throws IOException {
		logger.log("Sending response: " + response.toJSONString());
		OutputStreamWriter writer = new OutputStreamWriter(output, "UTF-8");
		writer.write(response.toJSONString());
		writer.close();
	}

	protected DynamoDB connectToDB() {
		AmazonDynamoDB client = DB_CLIENT_BUILDER.build();
		DynamoDB dynamoDB = new DynamoDB(client);
		logger.log("Connected to DB: " + (dynamoDB != null));
		return dynamoDB;
	}

	private static class Stop implements Comparable<Stop> {
		double distance;
		String title;
		String stopId;
		double lat;
		double lon;

		public Stop(double distance, String title, String stopId, double lat, double lon) {
			this.distance = distance;
			this.title = title;
			this.stopId = stopId;
			this.lat = lat;
			this.lon = lon;
		}

		@SuppressWarnings("unchecked")
		public JSONObject toJson() {
			JSONObject obj = new JSONObject();
			obj.put("distance", distance);
			obj.put("title", title);
			obj.put("stopId", stopId);
			obj.put("lat", lat);
			obj.put("lon", lon);
			return obj;
		}

		@Override
		public String toString() {
			return "Stop [distance=" + distance + ", title=" + title + ", stopId=" + stopId + "]";

		}

		@Override
		public int compareTo(Stop o) {
			if (this.distance > o.distance)
				return 1;
			if (this.distance < o.distance)
				return -1;
			return 0;
		}

	}

	/**
	 * Get stops in circle around the location of the user
	 *
	 * @param dynamoDB
	 * @param lon
	 * @param lat
	 * @param radius
	 *            in km
	 * @return
	 */
	private Queue<Stop> getStops(DynamoDB dynamoDB, double lon, double lat) {
		double min_lat = lat - Math.toDegrees(radius / R);
		double max_lat = lat + Math.toDegrees(radius / R);

		double min_lon = lon - Math.toDegrees(Math.asin(radius / R) / Math.cos(Math.toRadians(lat)));
		double max_lon = lon + Math.toDegrees(Math.asin(radius / R) / Math.cos(Math.toRadians(lat)));

		logger.log("min lat: " + min_lat + " max lat: " + max_lat);
		logger.log("min lon: " + min_lon + " max lon: " + max_lon);

		// check for stops in square around the location of the user
		ScanSpec scanSpec = new ScanSpec().withProjectionExpression("title, lat, lon, stopId")
				.withFilterExpression("#lat between :min_lat and :max_lat and #lon between :min_lon and :max_lon")
				.withNameMap(new NameMap().with("#lat", "lat").with("#lon", "lon"))
				.withValueMap(new ValueMap().withNumber(":min_lat", min_lat).withNumber(":max_lat", max_lat)
						.withNumber(":min_lon", min_lon).withNumber(":max_lon", max_lon));

		PriorityQueue<Stop> stopsInRadius = new PriorityQueue<Stop>();

		Table table = dynamoDB.getTable("Stops");

		try {
			Iterator<Item> iter = getItemIterator(table, scanSpec);
			while (iter.hasNext()) {
				Item item = iter.next();

				double distance = getDistance(lat, lon, item.getDouble("lat"), item.getDouble("lon"));
				logger.log("Stop " + item.getString("title") + ":" + distance + "km");

				// check for stops in circle around the location of the user
				if (distance >= 0) {
					stopsInRadius.add(new Stop(distance, item.getString("title"), item.getString("stopId"),
							item.getDouble("lat"), item.getDouble("lon")));
				}
			}

		} catch (Exception e) {
			logger.log("Unable to scan the Stops table: " + e.getMessage());
		}

		return stopsInRadius;

	}

	protected Iterator<Item> getItemIterator(Table table, ScanSpec spec) {
		ItemCollection<ScanOutcome> items = table.scan(spec);
		return items.iterator();
	}

	/**
	 * Gets the distance between the user and a Stop
	 * 
	 * @param mylat
	 * @param mylon
	 * @param stoplat
	 * @param stoplon
	 * @param radius
	 * @return
	 */
	private double getDistance(double mylat, double mylon, double stoplat, double stoplon) {
		double R = 6371;
		double dLat = Math.toRadians(mylat - stoplat);
		double dLon = Math.toRadians(mylon - stoplon);
		double a = Math.sin(dLat / 2.0) * Math.sin(dLat / 2.0) + Math.cos(Math.toRadians(mylat))
				* Math.cos(Math.toRadians(stoplat)) * Math.sin(dLon / 2.0) * Math.sin(dLon / 2.0);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = R * c;

		if (d <= radius)
			return d;
		else
			return -1;
	}

}
