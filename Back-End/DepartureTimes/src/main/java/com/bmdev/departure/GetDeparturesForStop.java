package com.bmdev.departure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * This lambda function retrieves real-time predictions of departures from a
 * certain stop (bus station) using the Next-Bus API.<br>
 * <br>
 * Terminology: <br>
 * <br>
 * Agency - An agency managing a set of bus routes <br>
 * Route - A list of stops of which a subset is covered by a bus. <br>
 * Direction - Ordered list of stops/stations visited by a bus. The bus visits
 * theses stops from start to end.<br>
 * <br>
 * Implementation: <br>
 * <br>
 * It expects to receive a GET request in the form of a JSON object over the
 * InputStream passed to its entry-point method "handleRequest". In the
 * "queryStringParameters" of this request it expects a String stopId
 * identifying the bus station.
 * 
 * The function then calls the Next-Bus API to retrieve the predicted departures
 * for each agency in XML and parses them using a DOM parser.
 *
 * These predicted departure times are returned as an array of
 * {@link Prediction} objects with increasing departure times in the body of a
 * GET response sent back to the client.
 * 
 * In the case of an error the function returns an error response with the error
 * field set to the error message.
 * 
 * @author Heila Botha
 *
 */
public class GetDeparturesForStop implements RequestStreamHandler {
	private static final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	private static final String webServiceURL = "http://webservices.nextbus.com/service/publicXMLFeed?command=predictions&a=";
	private static LambdaLogger logger;

	@Override
	public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
		logger = context.getLogger();
		logger.log("Running Lambda function GetDeparturesForStop");

		// GET new response returned by this lambda function
		JSONObject response = getJSONResponse();

		// GET request string url parameters
		String stopId = "";
		String stopName = "";

		JSONObject qsp = getQueryStringParameters(input);
		if (qsp != null) {
			stopId = getValue(qsp, "stopId", stopId);
			stopName = getValue(qsp, "stopTitle", stopName);
			if (stopId.equals("")) {
				sendErrorResponse(response, "No StopId Specified", output);
				return;
			}
		} else {
			sendErrorResponse(response, "No QueryStringParameters Specified", output);
			return;
		}
		logger.log("QueryStringParameters: stopId: " + stopId + " stopTitle: " + stopName);

		// Heap of departures sorted by first departure time
		PriorityQueue<Prediction> departures = new PriorityQueue<Prediction>(new PredictionComparator());

		String agency = "sf-muni"; // TODO do for all agencies

		Element docRoot = getXMLDOMFromURL(webServiceURL + agency + "&stopId=" + stopId);
		if (docRoot != null) {
			// retrieve departures
			populateDepartures(docRoot, agency, stopId, departures);
			addDeparturesToResponse(response, departures);
		} else {
			sendErrorResponse(response, "Error retrieving departure times.", output);
			return;
		}

		// sends GET response with departure times
		sendResponse(response, output);
	}

	/**
	 * Reads & parses the QueryStringParameters {@link JSONObject} from the response
	 * sent over the input stream. These are the parameters sent to the lambda
	 * function by encoding them into the URL.
	 * 
	 * @param input
	 *            the {@link InputStream} receiving GET requests from the API
	 *            GateWay.
	 * @return {@link JSONObject} containing the url parameters or null if no such
	 *         object is present in the request.
	 */
	private JSONObject getQueryStringParameters(InputStream input) {
		try {
			JSONParser parser = new JSONParser();
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			JSONObject event = (JSONObject) parser.parse(reader);

			if (event.get("queryStringParameters") != null) {
				JSONObject qps = (JSONObject) event.get("queryStringParameters");
				return qps;
			}
		} catch (IOException | ParseException e) {
			logger.log("Could not parse QueryStringParameters: " + e.getMessage());
		}
		return null;
	}

	/**
	 * Retrieves XML from URL and creates and parses the XML into a DOM.
	 * 
	 * @param url
	 *            URL from which to retrieve XML stream.
	 * @return the Document's root element or null if an exception occurred.
	 */
	protected Element getXMLDOMFromURL(String url) {

		InputSource xmlIS = getInputSourceForUrl(url);
		if (xmlIS == null) {
			return null;
		}
		Element docRoot = null;
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document dom = builder.parse(xmlIS);
			docRoot = dom.getDocumentElement();
			logger.log("Successfully parsed DOM of XML stream");
		} catch (SAXException | IOException | ParserConfigurationException e) {
			logger.log("Could not parse XML Document Stream:" + e.getMessage());
		}

		return docRoot;
	}

	/**
	 * Populates departures by parsing the XML in XMLDOMRoot.
	 * 
	 * @param XMLDOMRoot
	 * @param agency
	 *            The title of the agency for which we are retrieving departures
	 * @param stopId
	 *            The StopId of the stop for which we are retrieving departures.
	 * @param departures
	 *            The queue we populate with the departure predictions.
	 */
	private void populateDepartures(Element XMLDOMRoot, String agency, String stopId, Queue<Prediction> departures) {

		NodeList routeList = XMLDOMRoot.getElementsByTagName("predictions");
		if (routeList != null && routeList.getLength() > 0) {
			logger.log("Found " + routeList.getLength() + " routes.");
			for (int i = 0; i < routeList.getLength(); i++) {
				parseRoute(departures, routeList.item(i), agency);
			}
		}
		logger.log("Parsed " + departures.size() + " departures from website.");
	}

	/**
	 * Parses a route XML element called "predictions". The method does not throw
	 * exceptions but prints logs if something goes wrong. In this case departures
	 * are not populated.
	 * 
	 * @param departures
	 *            Queue to populate with departure times
	 * @param route
	 *            The Node to parse for route information
	 * @param agency
	 *            The agency we are currently parsing departure time for
	 */
	private void parseRoute(Queue<Prediction> departures, Node route, String agency) {
		// route info
		String routeTitle = getAttribute(route, "routeTitle");
		String routeTag = getAttribute(route, "routeTag");

		if (routeTitle != null && routeTag != null) {
			// predictions for route
			NodeList directions = route.getChildNodes();
			if (directions != null && directions.getLength() > 0) {
				logger.log("Found " + directions.getLength() + " directions for route " + routeTitle);

				for (int d = 0; d < directions.getLength(); d++) {
					parseDirection(departures, directions.item(d), agency, routeTitle, routeTag);
				}
			}
		}
	}

	/**
	 * Parses a direction XML element. The method does not throw exceptions but
	 * prints logs if something goes wrong. In this case departures are not
	 * populated.
	 * 
	 * @param departures
	 *            Queue to populate with departure times
	 * @param direction
	 *            The Node to parse for direction information
	 * @param agency
	 *            The agency we are currently parsing departure time for
	 */
	private void parseDirection(Queue<Prediction> departures, Node direction, String agency, String routeTitle,
			String routeTag) {
		if (direction.getNodeName().equals("direction")) {
			String directionTitle = getAttribute(direction, "title");
			if (directionTitle != null) {
				NodeList predictionNodes = direction.getChildNodes();
				Prediction p = parsePrediction(routeTitle, routeTag, directionTitle, agency, predictionNodes);
				if (p != null) {
					logger.log("Adding prediction" + p);
					departures.add(p);
				}
			}
		}
	}

	/**
	 * Parses a list of prediction XML elements for a direction. The method does not
	 * throw exceptions but prints logs if something goes wrong. In this case
	 * departures are not populated.
	 * 
	 * @param departures
	 *            Queue to populate with departure times
	 * @param predictions
	 *            The NodeList to parse for predicted departure times
	 * @param agency
	 *            The agency we are currently parsing departure times for
	 */
	private Prediction parsePrediction(String routeTitle, String routeTag, String direction, String agency,
			NodeList predictions) {
		if (predictions != null && predictions.getLength() > 0) {
			logger.log("Found " + predictions.getLength() + " predictions for " + routeTitle + "in direction "
					+ direction);
			try {
				int count = 0;
				for (int p = 0; p < predictions.getLength(); p++) {
					Node predictionNode = predictions.item(p);
					if (predictionNode.getNodeName().equals("prediction")) {
						count++;
					}
				}
				if (count > 0) {
					long[] prediction = new long[count];
					for (int p = 0, c = 0; p < predictions.getLength(); p++) {
						Node predictionNode = predictions.item(p);
						if (predictionNode.getNodeName().equals("prediction")) {
							String secondsString = getAttribute(predictionNode, "seconds");
							prediction[c++] = Long.parseLong(secondsString);
						}
					}
					return new Prediction(prediction, routeTitle, routeTag, agency, direction);
				}
			} catch (Exception e) {
				logger.log("Could not parse prediction: " + e.getMessage());
			}
		}
		return null;
	}

	/**
	 * Opens connection to URL and created a buffered {@link InputSource} for the
	 * contents
	 * 
	 * @param urlString
	 *            the URL to connect to
	 * @return an {@link InputSource} or null if connection failed
	 */
	protected InputSource getInputSourceForUrl(String urlString) {
		URL url = null;
		InputSource inputSource = null;
		try {
			url = new URL(urlString);
			URLConnection urlConnection = url.openConnection();
			urlConnection.setConnectTimeout(120000);
			urlConnection.setReadTimeout(120000);
			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
			inputSource = new InputSource(reader);
			logger.log("Connected to URL " + urlString);
		} catch (IOException e) {
			logger.log("Could not retrieve input source from " + urlString + " " + e.getMessage());
		}
		return inputSource;
	}

	@SuppressWarnings("unchecked")
	private void sendErrorResponse(JSONObject response, String message, OutputStream output) {
		response.put("statusCode", "400");
		response.put("error", message);
		sendResponse(response, output);
	}

	@SuppressWarnings("unchecked")
	private void addDeparturesToResponse(JSONObject response, Queue<Prediction> departures) {

		JSONObject responseBody = new JSONObject();
		JSONArray departuresJSONArray = new JSONArray();

		for (Prediction p : departures) {
			departuresJSONArray.add(p.toJSONObject());
		}

		responseBody.put("departures", departuresJSONArray);
		response.put("body", responseBody.toString());
	}

	@SuppressWarnings("unchecked")
	private JSONObject getJSONResponse() {
		JSONObject responseJson = new JSONObject();
		responseJson.put("statusCode", "200");

		JSONObject headerJson = new JSONObject();
		headerJson.put("Access-Control-Allow-Origin", "*");
		headerJson.put("Content-Type", "application/json");

		responseJson.put("headers", headerJson);

		return responseJson;
	}

	private void sendResponse(JSONObject response, OutputStream output) {
		logger.log(response.toJSONString());
		OutputStreamWriter writer = null;
		try {
			writer = new OutputStreamWriter(output, "UTF-8");
			writer.write(response.toJSONString());

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null)
				try {
					writer.close();
				} catch (IOException e) {
				}
		}
	}

	/**
	 * Gets the value of key in a JSONObject
	 * 
	 * @param object
	 * @param key
	 * @param defaultValue
	 * @return returns the value as a string or default value if not found
	 */
	private String getValue(JSONObject object, String key, String defaultValue) {
		if (object.get(key) != null) {
			return (String) object.get(key);
		}
		return defaultValue;
	}

	/**
	 * Utility method to get the value of an XML attribute {@link Node} or null id
	 * no such attribute exists.
	 * 
	 * @param node
	 * @param key
	 * @return
	 */
	private String getAttribute(Node node, String key) {
		NamedNodeMap attributes = node.getAttributes();
		if (attributes == null)
			return null;
		Node attribute = node.getAttributes().getNamedItem(key);
		if (attribute != null) {
			return attribute.getNodeValue();
		} else {
			return null;
		}
	}

	/**
	 * Compare {@link Prediction} objects by their first departure time
	 */
	private static class PredictionComparator implements Comparator<Prediction> {
		@Override
		public int compare(Prediction o1, Prediction o2) {
			if (o1.seconds[0] > o2.seconds[0])
				return 1;
			if (o1.seconds[0] < o2.seconds[0])
				return -1;
			return 0;
		}
	}

	/**
	 * Stores the predicted departure times at a stop for a specific agency on a
	 * specific route and direction.
	 */
	private static class Prediction {
		long[] seconds;
		String route;
		String agency;
		String direction;
		@SuppressWarnings("unused")
		String routeTag;

		/**
		 * NB seconds must at least contain a single time
		 * 
		 * @param seconds
		 * @param route
		 * @param agency
		 * @param direction
		 */
		public Prediction(long[] seconds, String route, String routeTag, String agency, String direction) {
			assert seconds.length > 0;
			this.seconds = seconds;
			this.route = route;
			this.routeTag = routeTag;
			this.agency = agency;
			this.direction = direction;
		}

		public JSONObject toJSONObject() {
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(this);

			JSONParser parser = new JSONParser();
			JSONObject jsonObj = new JSONObject();
			try {
				jsonObj = (JSONObject) parser.parse(json);
			} catch (ParseException e) {
				logger.log("Error converting object to JSON: " + this.toString());
			}
			return jsonObj;
		}

		@Override
		public String toString() {
			return "Prediction [route=" + route + ", agency=" + agency + ", direction=" + direction + ", seconds="
					+ Arrays.toString(seconds) + "]";
		}
	}

}
