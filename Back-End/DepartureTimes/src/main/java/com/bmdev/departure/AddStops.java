package com.bmdev.departure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;

/**
 * {@link AddStops} is a lambda function that adds all stops of a Route to the
 * DynamoDB. It receives the the Agency tag and the Route tag from the SNS
 * message.
 * 
 * 
 * @author Heila Botha
 *
 */
public class AddStops implements RequestHandler<SNSEvent, String> {
	/** Region the lambda function and DB is running */
	private static final Region REGION = Region.getRegion(Regions.US_EAST_1);

	/** XML Parser to parse the Route XML */
	private static final SAXParserFactory SAX_FACTORY = SAXParserFactory.newInstance();

	/** URL from where to obtain Route config XML */
	private static final String GET_ROUTE_CONFIG_URL = "http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=";

	/** Builder for Amazon DynamoDB clients */
	private static final AmazonDynamoDBClientBuilder DB_CLIENT_BUILDER = AmazonDynamoDBClientBuilder.standard()
			.withRegion(REGION.getName());

	/** Write log to CloudWatch */
	private static LambdaLogger logger;

	@Override
	public String handleRequest(SNSEvent event, Context context) {
		logger = context.getLogger();
		String message = event.getRecords().get(0).getSNS().getMessage();
		if (message == null || message.length() == 0) {
			logger.log("Parsing Route stops failed: no message specified");
			return "Fail";
		}
		String[] args = message.split(" ");
		if (args.length != 2) {
			logger.log("Parsing Route stops failed: incorrect number of message parameters");
			return "Fail";
		}

		logger.log("Running Lamdba Function AddStops with Input: " + message);

		SAXParser parser;
		int numStops = 0;
		String agencyTag = args[0];
		String routeTag = args[1];
		try {
			DynamoDB db = connectToDB();
			InputSource inputSource = getInputSourceForUrl(
					GET_ROUTE_CONFIG_URL + agencyTag + "&r=" + routeTag + "&terse=true&useForUI=false");
			logger.log("Got URL inputSource:" + (inputSource != null));

			parser = SAX_FACTORY.newSAXParser();
			logger.log("SaxParser instance created: " + (parser != null));

			StopsParser stopsParser = new StopsParser(db);
			parser.parse(inputSource, stopsParser);
			numStops = stopsParser.getCount();

		} catch (ParserConfigurationException | SAXException | IOException e) {
			logger.log("Adding Stops failed:" + e.getMessage());
			e.printStackTrace();
			return "Fail: " + numStops;
		}

		return "Success: " + numStops;

	}

	/**
	 * Opens connection to URL and created a buffered {@link InputSource} for the
	 * contents
	 * 
	 * @param urlString
	 *            the URL to connect to
	 * @return an {@link InputSource} or exception if the connection failed.
	 */
	private InputSource getInputSourceForUrl(String urlString) throws UnsupportedEncodingException, IOException {
		URL url = new URL(urlString);
		URLConnection urlConnection = url.openConnection();
		urlConnection.setConnectTimeout(120000);
		urlConnection.setReadTimeout(120000);
		BufferedReader geoLeocationDetails = new BufferedReader(
				new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
		InputSource inputSource = new InputSource(geoLeocationDetails);
		logger.log("Connected to URL:  " + (inputSource != null));
		return inputSource;
	}

	/**
	 * Connects to the DynamoDB
	 * 
	 * @param region
	 *            The region the DB is running
	 * @return
	 */
	protected DynamoDB connectToDB() {
		AmazonDynamoDB client = DB_CLIENT_BUILDER.build();
		DynamoDB dynamoDB = new DynamoDB(client);
		logger.log("Connected to DB: " + (dynamoDB != null));
		return dynamoDB;
	}

	/**
	 * Add Stop objects of this Route to the Database
	 *
	 */
	private static class StopsParser extends DefaultHandler {
		private final static String tableName = "Stops";

		private Table table;

		/** Number of Stops processed */
		private int count = 0;

		public StopsParser(DynamoDB dynamoDB) {
			table = dynamoDB.getTable(tableName);
			logger.log("Creating StopsParser for Stops");
		}

		public int getCount() {
			return count;
		}

		@Override
		public void startElement(String s, String s1, String elementName, Attributes attributes) throws SAXException {
			if (elementName.equalsIgnoreCase("stop")) {

				String title = attributes.getValue("title");
				String stopId = attributes.getValue("stopId");

				if (title != null && stopId != null) {

					String stopTag = attributes.getValue("tag");

					double lat = Double.parseDouble(attributes.getValue("lat"));
					double lon = Double.parseDouble(attributes.getValue("lon"));

					try {
						Item item = new Item().withPrimaryKey("stopId", stopId).withString("tag", stopTag)
								.withString("title", title).withDouble("lat", lat).withDouble("lon", lon);
						table.putItem(item);
						logger.log("Adding Stop " + item);
						count++;
					} catch (Exception e) {
						logger.log("Create Stop " + title + " failed.");
						logger.log(e.getMessage());
					}
				} else {
					if (title != null && stopId == null) {
						logger.log("Stop " + title + " has no stopId.");
					}
				}
			}
		}

	}

}
