package com.bmdev.departure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

/**
 * The {@link ProcessRoutes} lambda function receives an Agency tag in a SNS
 * message. It then retrieves a list of Routes for the Agency with this tag. For
 * each of these Routes it starts an {@link AddStops} lamdba function by sending
 * another SNS message. AddStops processes the list of Stops for the discovered
 * Route.
 * 
 * @author Heila Botha
 *
 */
public class ProcessRoutes implements RequestHandler<SNSEvent, String> {
	/** URL from where to obtain routeList for an Agency */
	private static final String GET_ROUTE_LIST_URL = "http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=";

	/** Region the lambda function is running */
	private static final Region REGION = Region.getRegion(Regions.US_EAST_1);

	/** SNS service to send messages to AddStops Lambda function */
	private static final String ARN_TOPIC = "arn:aws:sns:us-east-1:463511003771:stops";
	private static final AmazonSNS SNS_SERVICE = AmazonSNSClientBuilder.standard().withRegion(REGION.getName())
			.withCredentials(new AWSStaticCredentialsProvider(
					new BasicAWSCredentials("AKIAJBAIDB44W6I6N6CQ", "RM8pf1RvqHgvyd1Vf+8F/ullKCR6exDcnb6caClw")))
			.build();

	/** XML Parser to parse the list of routes from XML */
	private static final SAXParserFactory SAX_FACTORY = SAXParserFactory.newInstance();

	/** Write log to CloudWatch */
	private static LambdaLogger logger;

	@Override
	public String handleRequest(SNSEvent event, Context context) {
		logger = context.getLogger();

		String agency = event.getRecords().get(0).getSNS().getMessage();
		if (agency == null || agency.length() == 0) {
			logger.log("Parsing Routes failed: no agency specified in sns message");
			return "Fail";
		}

		logger.log("Running Lamdba Function ProcessRoutes with Input: " + agency);

		SAXParser parser;
		int numRoutes = 0;
		try {
			InputSource inputSource = getInputSourceForUrl(GET_ROUTE_LIST_URL + agency);
			logger.log("Got URL inputSource:" + (inputSource != null));

			parser = SAX_FACTORY.newSAXParser();
			logger.log("SaxParser instance created: " + (parser != null));

			RouteListParser routeHandler = new RouteListParser(agency);
			parser.parse(inputSource, routeHandler);
			numRoutes = routeHandler.getCount();

		} catch (ParserConfigurationException | SAXException | IOException e) {
			logger.log("Parsing Routes failed:" + e.getMessage());
			e.printStackTrace();
			return "Fail: " + numRoutes;
		}
		return "Success: " + numRoutes;
	}

	/**
	 * Opens connection to URL and created a buffered {@link InputSource} for the
	 * contents
	 * 
	 * @param urlString
	 *            the URL to connect to
	 * @return an {@link InputSource} or exception if the connection failed.
	 */
	private InputSource getInputSourceForUrl(String urlString) throws UnsupportedEncodingException, IOException {
		URL url = new URL(urlString);
		URLConnection urlConnection = url.openConnection();
		urlConnection.setConnectTimeout(120000);
		urlConnection.setReadTimeout(120000);
		BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
		InputSource inputSource = new InputSource(reader);
		logger.log("Connected to URL " + urlString);
		return inputSource;
	}

	/**
	 * Publish a message to the stops SNS topic to process the list of stops for
	 * this Agency and Route.
	 * 
	 * @param agencyTag
	 *            The tag uniquely identifying the Agency
	 * @param routeTag
	 *            Unique tag identifying the Route
	 */
	private static void getStops(String agency, String routeTag) {
		PublishRequest publishReq = new PublishRequest().withTopicArn(ARN_TOPIC).withMessage(agency + " " + routeTag);
		PublishResult result = SNS_SERVICE.publish(publishReq);

		logger.log("SNS Message published:" + result.getMessageId());
	}

	/**
	 * 
	 * Parses the title and tag of a Route from XML and fires the {@link AddStops}
	 * lambda function for parsing all Stops of this Route.
	 *
	 */
	private static class RouteListParser extends DefaultHandler {
		private final String agency;

		/** Number of Agencies processed */
		private int count = 0;

		public RouteListParser(String agency) {
			logger.log("Creating new RouteList parser for Agency " + agency);
			this.agency = agency;
		}

		public int getCount() {
			return count;
		}

		@Override
		public void startElement(String s, String s1, String elementName, Attributes attributes) throws SAXException {

			if (elementName.equalsIgnoreCase("route")) {

				String title = attributes.getValue("title");
				logger.log("Found new Route: " + title);

				String tag = attributes.getValue("tag");
				if (tag == null) {
					logger.log("Creating Route " + title + " failed.");
					return;
				}

				try {
					getStops(agency, tag);
					count++;
				} catch (Exception e) {
					logger.log("Creating Rgency " + title + " failed: " + e.getMessage());
				}
			}
		}

	}

}
