package com.bmdev.departure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.bmdev.departure.model.ScheduledEvent;

/**
 * {@link AddAgencies} is a lambda function that parses the list of Agencies
 * retrieved from the NextBus API as XML and then puts them in the Agency table
 * in a DynamoDB. It the fires an instance of the {@link ProcessRoutes} lambda
 * function for each Agency using the SNS messaging service.
 * 
 * It is fired by a Scheduled event in AWS that can be setup to fire monthly to
 * update the database.
 * 
 * 
 * @author Heila Botha
 *
 */
public class AddAgencies implements RequestHandler<ScheduledEvent, String> {
	/** URL from where to obtain agencyList */
	private static final String GET_AGENCY_LIST_URL = "http://webservices.nextbus.com/service/publicXMLFeed?command=agencyList";

	/** Region the lambda function and DB is running */
	private static final Region REGION = Region.getRegion(Regions.US_EAST_1);

	/** SNS service to send messages to ProcessRoutes Lambda function */
	private static final String ARN_TOPIC = "arn:aws:sns:us-east-1:463511003771:routes";
	private static final AmazonSNS SNS_SERVICE = AmazonSNSClientBuilder.standard().withRegion(REGION.getName())
			.withCredentials(new AWSStaticCredentialsProvider(
					new BasicAWSCredentials("AKIAJBAIDB44W6I6N6CQ", "RM8pf1RvqHgvyd1Vf+8F/ullKCR6exDcnb6caClw")))
			.build();

	/** XML Parser to parse the list of agencies from XML */
	private static final SAXParserFactory SAX_FACTORY = SAXParserFactory.newInstance();

	/** Builder for Amazon DynamoDB clients */
	private static final AmazonDynamoDBClientBuilder DB_CLIENT_BUILDER = AmazonDynamoDBClientBuilder.standard()
			.withRegion(REGION.getName());

	/** Write log to CloudWatch */
	private static LambdaLogger logger;

	@Override
	public String handleRequest(ScheduledEvent input, Context context) {
		logger = context.getLogger();
		logger.log("Running lamdba function AddAgencies with Input: " + input);

		SAXParser parser;
		int numAgencies = 0;
		try {
			DynamoDB db = connectToDB();
			InputSource inputSource = getInputSourceForUrl(GET_AGENCY_LIST_URL);
			logger.log("Got URL inputSource:" + (inputSource != null));

			parser = SAX_FACTORY.newSAXParser();
			logger.log("SaxParser instance created: " + (parser != null));

			AgencyParser agencyHandler = new AgencyParser(db);
			parser.parse(inputSource, agencyHandler);
			numAgencies = agencyHandler.getCount();

		} catch (ParserConfigurationException | SAXException | IOException e) {
			logger.log("Adding Agencies failed:" + e.getMessage());
			e.printStackTrace();
			return "Fail: " + numAgencies;
		}
		return "Success: " + numAgencies;
	}

	/**
	 * Opens connection to URL and created a buffered {@link InputSource} for the
	 * contents
	 * 
	 * @param urlString
	 *            the URL to connect to
	 * @return an {@link InputSource} or exception if the connection failed.
	 */
	protected InputSource getInputSourceForUrl(String urlString) throws UnsupportedEncodingException, IOException {
		URL url = new URL(urlString);
		URLConnection urlConnection = url.openConnection();
		urlConnection.setConnectTimeout(120000);
		urlConnection.setReadTimeout(120000);
		BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
		InputSource inputSource = new InputSource(reader);
		logger.log("Connected to URL " + (inputSource != null));
		return inputSource;
	}

	/**
	 * Publish a message to the routes SNS topic to process the list of routes for
	 * this Agency.
	 * 
	 * @param agencyTag
	 *            The tag uniquely identifying the Agency
	 */
	protected static void processRoutes(String agencyTag) {
		PublishRequest publishReq = new PublishRequest().withTopicArn(ARN_TOPIC).withMessage(agencyTag);
		PublishResult result = SNS_SERVICE.publish(publishReq);
		logger.log("SNS Message published:" + result.getMessageId());
	}

	/**
	 * Connects to the DynamoDB
	 * 
	 * @param region
	 *            The region the DB is running
	 * @return
	 */
	protected DynamoDB connectToDB() {
		AmazonDynamoDB client = DB_CLIENT_BUILDER.build();
		DynamoDB dynamoDB = new DynamoDB(client);
		logger.log("Connected to DB: " + (dynamoDB != null));
		return dynamoDB;
	}

	/**
	 * 
	 * Parses the title and tag of an Agency from XML and fires the ProcessRoutes
	 * lambda function for each parsed Agency
	 *
	 */
	private static class AgencyParser extends DefaultHandler {
		private final static String TABLE_NAME = "Agency";

		private Table table;

		/** Number of Agencies processed */
		private int count = 0;

		public AgencyParser(DynamoDB dynamoDB) {
			logger.log("Creating new Agency parser for table Agency");
			table = dynamoDB.getTable(TABLE_NAME);
		}

		public int getCount() {
			return count;
		}

		@Override
		public void startElement(String s, String s1, String elementName, Attributes attributes) throws SAXException {
			if (elementName.equalsIgnoreCase("agency")) {

				String title = attributes.getValue("title");
				String tag = attributes.getValue("tag");
				if (title != null && tag != null) {
					logger.log("Found new Agency: " + title);
					try {
						Item item = new Item().withPrimaryKey("tag", tag).withString("title", title);
						table.putItem(item);
						processRoutes(tag);
						count++;
					} catch (Exception e) {
						logger.log("Creating Agency " + title + " failed: " + e.getMessage());
					}
				} else {
					logger.log("Error adding Agency title: " + title + " tag: " + tag);
				}
			}
		}

	}

}
