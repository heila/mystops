package com.bmdev.departure;

import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.amazonaws.regions.Region;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.lambda.runtime.Context;

/**
 * A simple test harness for locally invoking your Lambda function handler.
 */
public class GetStopsForLocationHandlerTest {

	private static final String SAMPLE_INPUT_STRING = "{\"queryStringParameters\": {\"lat\":\"37.8078399\", \"lon\": \"-122.41081\"}}";
	private static final String EXPECTED_OUTPUT_STRING = "{\"headers\":{\"Access-Control-Allow-Origin\":\"*\",\"Content-Type\":\"application\\/json\"},\"body\":\"{\\\"stops\\\":[{\\\"distance\\\":0.0,\\\"stopId\\\":\\\"13095\\\",\\\"lon\\\":-122.41081,\\\"title\\\":\\\"Beach St & Stockton St\\\",\\\"lat\\\":37.8078399}]}\",\"statusCode\":\"200\"}";

	static void setFinalStatic(Field field, Object newValue) throws Exception {
		field.setAccessible(true);
		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
		field.set(null, newValue);
	}

	private Iterator<Item> getIteratorOfItems() {
		List<Item> items = new ArrayList<>();
		Item it = new Item();
		it.with("title", "Beach St & Stockton St");
		it.with("lat", "37.8078399");
		it.with("lon", "-122.41081");
		it.with("stopId", "13095");
		items.add(it);
		return items.iterator();
	}

	@Test
	public void testGetStopsForLocationHandler() throws NoSuchFieldException, SecurityException, Exception {
		GetStopsForLocationHandler handler = new GetStopsForLocationHandler();
		GetStopsForLocationHandler spyhandler = spy(handler);

		doReturn(getMockDB()).when(spyhandler).connectToDB();
		doReturn(getIteratorOfItems()).when(spyhandler).getItemIterator((Table) notNull(), (ScanSpec) notNull());

		InputStream input = new ByteArrayInputStream(SAMPLE_INPUT_STRING.getBytes());
		OutputStream output = new ByteArrayOutputStream();

		Context ctx = createContext();
		spyhandler.handleRequest(input, output, ctx);

		String sampleOutputString = output.toString();
		System.out.println(sampleOutputString);
		Assert.assertEquals(EXPECTED_OUTPUT_STRING, sampleOutputString);
	}

	private Context createContext() {
		TestContext ctx = new TestContext();
		ctx.setFunctionName("handleRequest");
		return ctx;
	}

	private DynamoDB getMockDB() throws NoSuchFieldException, SecurityException, Exception {
		Region regionMock = mock(Region.class);
		doReturn("US-East-1").when(regionMock).getName();
		setFinalStatic(GetStopsForLocationHandler.class.getDeclaredField("REGION"), regionMock);
		DynamoDB dbMock = mock(DynamoDB.class);
		Table tableMock = mock(Table.class);
		doReturn(tableMock).when(dbMock).getTable("Stops");
		return dbMock;
	}

}
