package com.bmdev.departure;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.amazonaws.regions.Region;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.lambda.runtime.events.SNSEvent.SNS;
import com.amazonaws.services.lambda.runtime.events.SNSEvent.SNSRecord;

/**
 * A simple test harness for locally invoking your Lambda function handler.
 */
public class AddStopsTest {

	private static SNSEvent input;

	@BeforeClass
	public static void createInput() throws IOException {
		SNS sns = new SNS();
		sns.setMessage("sf-muni E");
		input = new SNSEvent();
		List<SNSEvent.SNSRecord> list = new ArrayList<SNSEvent.SNSRecord>();
		SNSRecord r = new SNSRecord();
		r.setSns(sns);
		list.add(r);
		input.setRecords(list);
	}

	private Context createContext() {
		TestContext ctx = new TestContext();
		ctx.setFunctionName("handleRequest");
		return ctx;
	}

	static void setFinalStatic(Field field, Object newValue) throws Exception {
		field.setAccessible(true);
		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
		field.set(null, newValue);
	}

	@Test
	public void testAddAgenciesHandler() throws Exception {
		Context ctx = createContext();
		AddStops handler = new AddStops();
		AddStops spyhandler = spy(handler);

		Region regionMock = mock(Region.class);
		doReturn("US-East-1").when(regionMock).getName();
		setFinalStatic(AddAgencies.class.getDeclaredField("REGION"), regionMock);
		DynamoDB dbMock = mock(DynamoDB.class);
		Table tableMock = mock(Table.class);
		doReturn(tableMock).when(dbMock).getTable("Stops");
		doReturn(dbMock).when(spyhandler).connectToDB();

		String output = spyhandler.handleRequest(input, ctx);
		System.out.println(output);
		Assert.assertEquals("Success: 33", output);

	}
}
