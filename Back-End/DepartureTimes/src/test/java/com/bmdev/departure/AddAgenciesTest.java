package com.bmdev.departure;

import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.amazonaws.regions.Region;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.bmdev.departure.model.ScheduledEvent;

public class AddAgenciesTest {

	private static ScheduledEvent input;

	@BeforeClass
	public static void createInput() throws IOException {
		input = new ScheduledEvent();
	}

	private Context createContext() {
		TestContext ctx = new TestContext();
		ctx.setFunctionName("handleRequest");
		return ctx;
	}

	public void mockSNS(Class<?> c, String fieldName) throws Exception {
		AmazonSNS snsService = Mockito.mock(AmazonSNS.class);
		PublishResult result = Mockito.mock(PublishResult.class);
		doReturn("42").when(result).getMessageId();
		doReturn(result).when(snsService).publish((PublishRequest) notNull());
		setFinalStatic(c.getDeclaredField(fieldName), snsService);
	}

	static void setFinalStatic(Field field, Object newValue) throws Exception {
		field.setAccessible(true);
		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
		field.set(null, newValue);
	}

	@Test
	public void testAddAgenciesHandler() throws Exception {
		Context ctx = createContext();
		AddAgencies handler = new AddAgencies();
		AddAgencies spyhandler = spy(handler);

		Region regionMock = mock(Region.class);
		doReturn("US-East-1").when(regionMock).getName();
		setFinalStatic(AddAgencies.class.getDeclaredField("REGION"), regionMock);
		DynamoDB dbMock = mock(DynamoDB.class);
		Table tableMock = mock(Table.class);
		doReturn(tableMock).when(dbMock).getTable("Agency");
		doReturn(dbMock).when(spyhandler).connectToDB();

		mockSNS(AddAgencies.class, "SNS_SERVICE");

		String output = spyhandler.handleRequest(input, ctx);
		System.out.println(output);
		Assert.assertEquals("Success: 63", output);

	}
}
