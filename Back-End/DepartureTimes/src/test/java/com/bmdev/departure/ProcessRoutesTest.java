package com.bmdev.departure;

import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.amazonaws.regions.Region;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.lambda.runtime.events.SNSEvent.SNS;
import com.amazonaws.services.lambda.runtime.events.SNSEvent.SNSRecord;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

public class ProcessRoutesTest {

	private static SNSEvent input;

	@BeforeClass
	public static void createInput() throws IOException {
		SNS sns = new SNS();
		sns.setMessage("sf-muni");
		input = new SNSEvent();
		List<SNSEvent.SNSRecord> list = new ArrayList<SNSEvent.SNSRecord>();
		SNSRecord r = new SNSRecord();
		r.setSns(sns);
		list.add(r);
		input.setRecords(list);
	}

	private Context createContext() {
		TestContext ctx = new TestContext();
		ctx.setFunctionName("handleRequest");
		return ctx;
	}

	public void mockSNS(Class<?> c, String fieldName) throws Exception {
		AmazonSNS snsService = Mockito.mock(AmazonSNS.class);
		PublishResult result = Mockito.mock(PublishResult.class);
		doReturn("42").when(result).getMessageId();
		doReturn(result).when(snsService).publish((PublishRequest) notNull());
		setFinalStatic(c.getDeclaredField(fieldName), snsService);
	}

	static void setFinalStatic(Field field, Object newValue) throws Exception {
		field.setAccessible(true);
		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
		field.set(null, newValue);
	}

	@Test
	public void testProcessRoutesHandler() throws Exception {
		Context ctx = createContext();
		ProcessRoutes handler = new ProcessRoutes();
		ProcessRoutes spyhandler = spy(handler);

		Region regionMock = mock(Region.class);
		doReturn("US-East-1").when(regionMock).getName();
		setFinalStatic(ProcessRoutes.class.getDeclaredField("REGION"), regionMock);

		mockSNS(ProcessRoutes.class, "SNS_SERVICE");

		String output = spyhandler.handleRequest(input, ctx);
		System.out.println(output);
		Assert.assertEquals("Success: 82", output);
	}
}
