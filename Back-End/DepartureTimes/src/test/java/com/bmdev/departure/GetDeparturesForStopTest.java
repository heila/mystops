package com.bmdev.departure;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.InputSource;

import com.amazonaws.services.lambda.runtime.Context;

public class GetDeparturesForStopTest {
	private static final String webServiceURL = "http://webservices.nextbus.com/service/publicXMLFeed?command=predictions&a=";

	private static final String CORRECT_INPUT_STRING = "{\"queryStringParameters\": {\"stopId\":\"14510\", \"stopTitle\": \"Embarcadero Folsom St\"}}";
	private static final String ERROR_INPUT_STRING = "{}";
	private static final String ERROR_INPUT_STRING_2 = "{\"queryStringParameters\": {}}";;

	private static final String EXPECTED_ERROR_OUTPUT_STRING = "{\"headers\":{\"Access-Control-Allow-Origin\":\"*\",\"Content-Type\":\"application\\/json\"},\"error\":\"No QueryStringParameters Specified\",\"statusCode\":\"400\"}";
	private static final String EXPECTED_ERROR_OUTPUT_STRING_2 = "{\"headers\":{\"Access-Control-Allow-Origin\":\"*\",\"Content-Type\":\"application\\/json\"},\"error\":\"No StopId Specified\",\"statusCode\":\"400\"}";
	private static final String EXPECTED_ERROR_DOM_NULL = "{\"headers\":{\"Access-Control-Allow-Origin\":\"*\",\"Content-Type\":\"application\\/json\"},\"error\":\"Error retrieving departure times.\",\"statusCode\":\"400\"}";
	private static final String EXPECTED_CORRECT_OUTPUT_MOCK_URL = "{\"headers\":{\"Access-Control-Allow-Origin\":\"*\",\"Content-Type\":\"application\\/json\"},\"body\":\"{\\\"departures\\\":[{\\\"seconds\\\":[1796,2636],\\\"route\\\":\\\"KT-Ingleside\\\\\\/Third Street\\\",\\\"routeTag\\\":\\\"KT\\\",\\\"agency\\\":\\\"sf-muni\\\",\\\"direction\\\":\\\"Outbound to Folsom & Embarcadero\\\"},{\\\"seconds\\\":[1974,2754],\\\"route\\\":\\\"N-Judah\\\",\\\"routeTag\\\":\\\"N\\\",\\\"agency\\\":\\\"sf-muni\\\",\\\"direction\\\":\\\"Outbound to Ocean Beach\\\"},{\\\"seconds\\\":[1968,2808],\\\"route\\\":\\\"L-Taraval\\\",\\\"routeTag\\\":\\\"L\\\",\\\"agency\\\":\\\"sf-muni\\\",\\\"direction\\\":\\\"Outbound to S.F. Zoo\\\"},{\\\"seconds\\\":[3919,5059],\\\"route\\\":\\\"KT-Ingleside\\\\\\/Third Street\\\",\\\"routeTag\\\":\\\"KT\\\",\\\"agency\\\":\\\"sf-muni\\\",\\\"direction\\\":\\\"Outbound to Balboa Park Station\\\"}]}\",\"statusCode\":\"200\"}";
	private static final String EXPECTED_CORRECT_OUTPUT_MOCK_URL_ERROR = "{\"headers\":{\"Access-Control-Allow-Origin\":\"*\",\"Content-Type\":\"application\\/json\"},\"body\":\"{\\\"departures\\\":[{\\\"seconds\\\":[1974],\\\"route\\\":\\\"N-Judah\\\",\\\"routeTag\\\":\\\"N\\\",\\\"agency\\\":\\\"sf-muni\\\",\\\"direction\\\":\\\"Outbound to Ocean Beach\\\"}]}\",\"statusCode\":\"200\"}";

	private Context createContext() {
		TestContext ctx = new TestContext();
		ctx.setFunctionName("handleRequest");
		return ctx;
	}

	@Test
	public void testGetDeparturesForStop() throws IOException {
		GetDeparturesForStop handler = new GetDeparturesForStop();

		InputStream input = new ByteArrayInputStream(CORRECT_INPUT_STRING.getBytes());
		OutputStream output = new ByteArrayOutputStream();

		handler.handleRequest(input, output, createContext());

		// validate output here if needed.
		String sampleOutputString = output.toString();
		System.out.println(sampleOutputString);
		// Assert.assertEquals(EXPECTED_OUTPUT_STRING, sampleOutputString);
	}

	@Test
	public void testMockURLConnection() throws IOException {
		GetDeparturesForStop handler = new GetDeparturesForStop();
		GetDeparturesForStop spyhandler = spy(handler);

		InputSource is = new InputSource(new FileInputStream("src/test/resources/publicXMLFeed.xml"));

		doReturn(is).when(spyhandler).getInputSourceForUrl(webServiceURL + "sf-muni" + "&stopId=" + 14510);

		InputStream input = new ByteArrayInputStream(CORRECT_INPUT_STRING.getBytes());
		OutputStream output = new ByteArrayOutputStream();

		spyhandler.handleRequest(input, output, createContext());

		// validate output here if needed.
		String sampleOutputString = output.toString();
		System.out.println(sampleOutputString);
		Assert.assertEquals(EXPECTED_CORRECT_OUTPUT_MOCK_URL, sampleOutputString);
	}

	@Test
	public void testMockURLConnectionErrorXML() throws IOException {
		GetDeparturesForStop handler = new GetDeparturesForStop();
		GetDeparturesForStop spyhandler = spy(handler);

		InputSource is = new InputSource(new FileInputStream("src/test/resources/publicXMLFeedError.xml"));

		doReturn(is).when(spyhandler).getInputSourceForUrl(webServiceURL + "sf-muni" + "&stopId=" + 14510);

		InputStream input = new ByteArrayInputStream(CORRECT_INPUT_STRING.getBytes());
		OutputStream output = new ByteArrayOutputStream();

		spyhandler.handleRequest(input, output, createContext());

		// validate output here if needed.
		String sampleOutputString = output.toString();
		System.out.println(sampleOutputString);
		Assert.assertEquals(EXPECTED_CORRECT_OUTPUT_MOCK_URL_ERROR, sampleOutputString);
	}

	@Test
	public void testNoQueryStringParameters() throws IOException {
		GetDeparturesForStop handler = new GetDeparturesForStop();

		InputStream input = new ByteArrayInputStream(ERROR_INPUT_STRING.getBytes());
		OutputStream output = new ByteArrayOutputStream();

		handler.handleRequest(input, output, createContext());

		String sampleOutputString = output.toString();
		// System.out.println(sampleOutputString);
		Assert.assertEquals(EXPECTED_ERROR_OUTPUT_STRING, sampleOutputString);
	}

	@Test
	public void testNoStopIdParameter() throws IOException {
		GetDeparturesForStop handler = new GetDeparturesForStop();

		InputStream input = new ByteArrayInputStream(ERROR_INPUT_STRING_2.getBytes());
		OutputStream output = new ByteArrayOutputStream();

		handler.handleRequest(input, output, createContext());

		String sampleOutputString = output.toString();
		// System.out.println(sampleOutputString);
		Assert.assertEquals(EXPECTED_ERROR_OUTPUT_STRING_2, sampleOutputString);
	}

	@Test
	public void testDomParsingFailed() throws IOException {

		GetDeparturesForStop handler = new GetDeparturesForStop();
		GetDeparturesForStop spyhandler = spy(handler);

		doReturn(null).when(spyhandler).getXMLDOMFromURL(webServiceURL + "sf-muni" + "&stopId=" + 14510);

		InputStream input = new ByteArrayInputStream(CORRECT_INPUT_STRING.getBytes());
		OutputStream output = new ByteArrayOutputStream();

		spyhandler.handleRequest(input, output, createContext());

		String sampleOutputString = output.toString();
		// System.out.println(sampleOutputString);
		Assert.assertEquals(EXPECTED_ERROR_DOM_NULL, sampleOutputString);
	}

}
