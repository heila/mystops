#MyStops
(Project: Departure Times)

Implements a service that gives real-time departure times for public transportation using a freely available public NextBus API (http://www.nextbus.com/).
##Introduction
Many people use public transport everyday to get to work or travel around within the city. The advantages include no parking problems or costs in cities such as San Francisco and London where parking is a real problem. 

When traveling, there are usually multiple possible routes one could take. These routes leave from different locations with different modes of transport. The best option is a combination of the distance to the departure point, departure time of the next applicable bus/train and the total trip time. 

In order to simplify this choice I developed an app that can present the user with
1. list of the closest stations,
2. real time departure times from each station.

When the user wants to travel, he/she takes out their mobile device or opens the app on their home/public PC. The application gets their location from the device and shows them the closest bus stops and the distance to each. When the user selects a specific stop, the app shows the list of real time scheduled busses departing from that location on all routes in all directions.

###High Level Requirements
1. The app must run on mobile phones and PC's.
2. The app must geolocalize the user to find departures from their closest bus stops.
3. The application must be scalable, have good performance and have a high level of availability

##Project Description

###Overview
In order to create a scalable, highly available web application, I used Amazon's Web Services (AWS) to create the application. I have not used any of the AWS services before, so I thought it would be a good learning experience. 

The application is designed to consist of the static web page front-end with which the user can interact. The web page  calls the  back-end exposing an RESTFul API.

![logo](img/architecture.png "architecture")


###Front-End
In order to run the application on mobile phones and PC's the front-end is designed as a static web page that runs on Amazon's Simple Storage Service (S3). S3 is a high performance object store to which files can be uploaded/downloaded. The service automatically scales to handle high traffic volumes eliminating the need for a custom load balancer. S3 also has a very high availability of 99.99%, is inexpensive to use.

The web page has four main responsibilities:

1. Get  the location of the user (or use default location if not possible). This can be retrieved from the HTML5's geolocalize API).  

2. Show the users their location to ensure it is correct and to update their location if inaccurate. In order to do this the position of the user is displayed as a pin on a map. This can be done using the Google Maps Javascript API. The pin can then be dragged in order to change the location of the user.

3. Retrieve a list of bus stops close to the location from the back-end API and display them to the user showing their distance from the user. This can be achieved by getting the current location of the pin showing the user's location and retrieving the bus stops in the region. These bus stops can then be showed as custom markers on the map.

4. Allow the user to select a bus stop and retrieve departure times from the back-end. The user can select a bus stop by clicking on the custom bus-stop marker. The website then calls back to the back-end API to retrieve a list of departure times for the stop.

###Back-End

The back-end is responsible for doing the heavy lifting for the web page. It must:

1. Get the bus stops near a location.
2. Retrieve departure times for the busses leaving from a specific bus stop.

Since these methods are stateless (they do not update the state of the back-end but only retrieve data) they are designed to be implemented as lambda functions. AWS Lambda functions allow you to create a serverless application by uploading and running code without having to provision or manage servers. You pay only for the runtime of the lambda functions so there are no additional costs when they are not running. They are inexpensive to use and scale automatically to accommodate different loads of traffic at high availability.

These functions can be triggered by different events, but I used AWS's API Gateway to expose them to the front-end using a RESTFul API.

In order to retrieve information about the location of bus stops and departure times the application calls the publicly available NextBus API.

####The NextBus API
NextBus provides an RESTFul API to retrieve information about bus routes, bus stops, real-time vehicle tracking and predicted departure times for a collection of bus agencies.

Below is a model of how the information from the API is structured and connected.
![logo](img/NextBus.png "UML diagram")

Each Agency has a set of bus routes that they service. Each Route contains a set of Directions and a list of Stops. The list of Stops includes all bus stops visited by busses on this Route. A Direction specifies an ordered subset of these stops that a bus following this Direction visits. A Direction is either inbound or outbound. At certain peak times there might for example be an express bus that follows a Direction that only visits selected stops. In other words a Route can have multiple possible Directions.

A Stop contains a stopTag that represents the bus stop and direction (inbound/outbound) for a specific Route. It also contains the stopId that uniquely identifies the bus stop and the location of the stop.

For a specific agency and a specific stopId, I can retrieve Predictions for all busses departing from a stop on different Routes and Directions passing through this stop.

The NextBus API exposes an API the retrieve the following information:

1. List of Agencies
2. List of Routes for each Agency
3. A detailed Route configuration including a list of Directions and list of Stops on the route.
4. Predictions for a specific stop and Agency 
Retrieve bus stops near a location
The list of all Stops can only be retrieved by iterating through all Agencies and retrieving the Route configurations for each Route associated with the Agency. From these Route configuration objects the list of Stops for the route can then be collected. 

The API currently contains 64 Agencies and ~15 000 Stops. Retrieving all stops in this way for each location change sent from the front-end would take too long to process. Therefore a database was added to store the list of Agencies and Stops for fast retrieval. This is possible since this information does not change regularly.

I can then create a lambda function that retrieves Stops from the database that fall in a certain distance from the location (sent by the front-end). These Stops are sorted by increasing distance from the location and a set of 5 closest stops are returned. 

####Populating and querying the database
DynamoDB is used to cache information to improve performance for the "Retrieve stops for location" lambda function. DynamoDB is a NoSQL database service that provides fast and predictable performance and that can scale automatically. I use a NoSQL database instead of a relational database since I only require two type of object in the table and they do not have a direct connection to each other.

I store the following tables of objects for quick lookup:

1. Agency : stores a list of agencies the NextBus API supports
2. Stops: stores the stopId and stopTitle for each bus stop

These tables are populated by three lambda functions: 

1. AddAgencies: Retrieves a list of Agencies from the NextBus API and adds them to the database. It also fires the ProcessRoutes lambda function to retrieve the list of Routes for each Agency. The AddAgencies lambda function uses the AWS SNS service to send messages that fire ProcessRoute lambda functions for each Agency.

2. ProcessRoutes: Retrieves a list of Routes from the NextBus API. For each Route it fires the AddStops lambda function with AWS SNS to retrieve the stops for a Route.

3. AddStops. Retrieves a Route configuration from the NextBus API and extracts the list of Stops. These Stops are then put in the database with their unique stop id, location (latitude and longitude) and stop title.

The XML returned from the NextBus API is parsed using SAX parsers to minimize the memory footprint of the lambda functions.

To retrieve the stops close to a location the database is scanned for Stops in a square around the location in question. This check is easy and quick to perform for each table entry. These stops are then filtered further by the lambda function who only returns Stops that fall in a specific radius around the location or the user.

####Retrieving departure times for a stop
The departures from a bus stop can be queried from the NextBus API using a stopId and agencyTag. The front end allows the user to retrieve departures for a specific stop. The stopId of this stop is then sent to the back-end to retrieve the departures for this stop for all agencies. The list of agencies are retrieved from the database. The predicted departure times for all Agencies are parsed from XML using a DOM parser and stored in a min heap to be returned to the front end in order of increasing time-until-departure in seconds.

####Testing & Monitoring
The lambda functions are written in Java 8 using a serverless project in Eclipse. The AWS SDK for Eclipse makes it simple to write JUnit tests to verify the behaviour of lambda functions. It provides a mock Input objects to the functions for common input events supported by the platform and a local DynamoDb to test the functionality against. 

I implemented a series of JUnit tests to check the behaviour of these functions especially in edge cases. To test certain behaviour I used Mockito to mock functions that retrieve the XML from the NextBus API and return our own XML stored in files in the resources directory.

In order to ensure the lambda functions are executing correctly and to debug the functions if something goes wrong I used the built-in Lambda logger to log the progress of the lambda functions. These logs are easily parsable and queryable using CloudWatch on AWS.

##Considerations, Conclusions & Future Work
The application is currently running on AWS in the US-East-1 Region at: https://s3.amazonaws.com/departures-hb-17/index.html.

![logo](img/app.png "deployed application")

###Lambda functions
As mentioned before the Lambda functions are implemented in Java since I did not want to take on learning a new language and new platform at the same time. In hindsight I would rather use one of the more lightweight languages supported by the platform such as NodeJS or Python. The main reason for this is that the lambda functions currently have a long "cold startup time". This is the time it takes to build the container the first time a lambda function is called. From what I can find on the Internet it seems this is related to the size of the deployed Lambda function (mine are quite big at ~18MB since Eclipse deploys all functions in one zip file.) My lambda functions use very little memory ~90MB  which could cause AWS to schedule them on small CPU machines and increase this cold start time to setup the container. This requires further investigation.

Another problem with the lambda functions is that they are limited to 5 minutes of execution time. This was a problem in the earlier versions of the lambda functions that populate the DB and caused them to timeout. Once again the functions use very little memory so they could possibly be scheduled on machines with small CPUs. When running these functions on my local machine they completed in under 5 minutes but they are obviously deployed on machines with less CPU power. I solved this problem by reducing the work done by a single CPU by distributing the processing of each Route configuration and its stops to a new lambda function.

###Database
The DynamoDB has to be updated when the NextBus agencies/stops are updated. This happens very infrequently but in this case the db has to be re-populated which can take up to 10 minutes. In this time the application might return misleading information. One option is to take the Database off-line while updating it.

###Edge cases
 - No real time data available for a stop:
 I display dialog to the user
 - No more next departures:
  I display dialog to the user
 - Closest stop is further away than the radius:
  I display dialog to the user
 - Too many close by stops:
 I only return the five closest stops
- Server initializing (NextBus):
  I display dialog to the user
###Production-readiness
Currently the lambda functions and web page are deployed manually. In the future I would like to set up continuous deployment and continuous integration to deploy frequently and run tests for each commit and deployment automatically.

###Security
Future work can investigate encrypting the location of the user sent over the Internet to the back-end. I do not support user login or store the location of the user on our servers so there is low risk of information leakage.

IAM roles on AWS also ensures that lambda functions and the API gateway only has permissions to access only the resources they require.

Denial of service attacks should also be investigated.

###Regions and Availability Zones
Future work include deploying the application in different Regions and Availability Zones to improve the availability of the application.



