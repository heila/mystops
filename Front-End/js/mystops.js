var map;
var redMarker;
var markers = [];
var defaultLat = 37.8078229469106;
var defaultLon = -122.411432272491;
var latCurrent = defaultLat;
var lonCurrent = defaultLon;

var HttpClient = function() {
    this.get = function(aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.open( "GET", aUrl, true );
        anHttpRequest.onreadystatechange = function() { 
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }
        anHttpRequest.send( null );
    }
}

function getStopsForLocation(lat, lon) {

    // get stops close by
    var client = new HttpClient();
    client.get('https://qpy2onpsqh.execute-api.us-east-1.amazonaws.com/DEV/stops?lat='+lat+'&lon='+lon, function(response) {
        var responseObj = JSON.parse(response);
        drawStops(responseObj.stops);
    });

    // set marker textbox to location
    document.getElementById("inputLocation").value = lat + ", " + lon;

    // clear the elements showing departure times
    document.getElementById("stopDetail").innerHTML = 'Select a stop to view real-time departures';
    document.getElementById("predlist").innerHTML = '';

    // reset map to new location
    map.setCenter(new google.maps.LatLng(lat, lon));
    map.setZoom(16);
    map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
    redMarker.setPosition(new google.maps.LatLng(lat, lon));
}

function drawStops(stops) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    } 
    markers = [];

    var image = {
        url: 'https://s3.amazonaws.com/departures-hb-17/img/busStop.png',
        size: new google.maps.Size(32, 32),
        zIndex:1000,
    };

    if (stops == null || stops.length == 0) {
        alert("No stops close to your location");
        return;
    }

    for (var i = 0; i < stops.length; i++) {
        var stop = stops[i];
        var marker = new google.maps.Marker({
            position: {lat: stop.lat, lng: stop.lon},
            map: map,
            icon: image,
            title: stop.title+' ('+ (stop.distance*1000).toFixed(0)+'m)',
            customInfo: stop.stopId,
            draggable: false
        });
        google.maps.event.addListener(marker, 'click', function() {
            var title = document.getElementById("stopDetail");
            title.innerHTML = 'Departures: '+this.title;
            getDeparturesForStop(this.title,this.customInfo);
        });
        markers.push(marker);
    }

    
}

function str_pad_left(string,pad,length) {
    return (new Array(length+1).join(pad)+string).slice(-length);
}

function drawPredictions(predictions) {
    var htmlElements = "";

    for (var i = 0; i < predictions.length; i++) {
        var pred = predictions[i];
        var minutes = Math.floor(pred.seconds[0] / 60);
        var seconds = pred.seconds[0] - minutes * 60;
        var finalTime = str_pad_left(minutes,'0',2)+'m'+str_pad_left(seconds,'0',2)+'s';
        htmlElements += '<li class="list-group-item"><span class="badge">'+finalTime+'</span>Route: '+pred.route+' ('+pred.direction+')</li>';
    }
    var container = document.getElementById("predlist");
    container.innerHTML = htmlElements;
    if (predictions == null || predictions.length == 0) {
        alert("No departures found");
        return;
    }
}

function getDeparturesForStop(title,stopId) {
    var client = new HttpClient();
    client.get('https://qpy2onpsqh.execute-api.us-east-1.amazonaws.com/DEV/departures?stopId='+stopId+'&stopTitle='+title, function(response) {
        var responseObj = JSON.parse(response);
        drawPredictions(responseObj.departures)
    });
}

function initMap() {
    var pos = new google.maps.LatLng(latCurrent, lonCurrent);
    var mapOptions = {
        center: pos,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    redMarker = new google.maps.Marker({
        position: pos,
        map: map,
        title: 'You are here',
        draggable: true
    });
    google.maps.event.addListener(redMarker, 'dragend', function () {
        latCurrent = this.getPosition().lat();
        lonCurrent = this.getPosition().lng();
        getStopsForLocation(latCurrent, lonCurrent);
    });

    getStopsForLocation(latCurrent, lonCurrent);

    var geolocate = document.getElementById("geolocate");
    geolocate.onclick = getLocation;
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showPositionError,  { timeout: 10000000});
    } else { 
        alert("Geolocation is not supported by this browser.");
    }
}

function showPositionError(position) {
  alert('Error occurred. Error code: ' + error.code);
}

function showPosition(position) {
    latCurrent = position.coords.latitude;
    lonCurrent = position.coords.longitude;
    getStopsForLocation(latCurrent, lonCurrent);
}

function showLocation() {
    latCurrent = defaultLat;
    lonCurrent = defaultLon;
    getStopsForLocation(latCurrent, lonCurrent);
}



